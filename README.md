# mget

This command-line node.js program fetches a remote file from a server in chunks, the size of which are specified as an option. The local destination file name can be selected with the **-o** flag. The default behavior is to download the first four 1Mbyte pieces of the file. The **-a** flag will cause the entire file to be downloaded in chunksize pieces, optionally with concurrent reads.

### Install
```
npm install
```

### Run
```
npm run-script start
```

or 
```node mget.js```

or just
```mget.js```

This project was developed using Visual Studio Code, there is a .vscode directory that contains run configurations.

### Test
```
npm test
```

### Usage

There are several defaults and options as shown with **mget -h**. Chunks can be requested from the server concurrently (the -p flag) but the default is one at a time. The URL of the file on the server must be supplied as the last command-line argument.

```
mget.js [-c chunksize_in_bytes] [-o outputfile] [-p num_reads_in_parallel] [-h]
<URL>

Positionals:
  URL  the URL of the file to download                                  [string]

Options:
  --version   Show version number                                      [boolean]
  -c          download chunk size in bytes                    [default: 1048576]
  -o          output file name                       [default: "outputFile.dat"]
  -i          remote file start index                               [default: 0]
  -m          max chunks to receive                                 [default: 4]
  -a          download entire file (overrides -m)                      [boolean]
  -p          parallel                                              [default: 1]
  -d          debug                                                    [boolean]
  -h, --help  Show help                                                [boolean]
```


### The Algorithm

  I went a little overboard on this solution, got interested in the problem and did more than was required. There are two queues, one that feeds the remote file chunk reader so that we can perform concurrent reads and one that feeds the local file writer with results of the reads. I couldn't find a good way to predetermine the total file size on the remote server so we simply read until we see a 416. This means that state has to be maintained so we can shut down the read queue and close the written file when everything is flushed.
  
  File writeStream would have been nice to use but apparently it cannot take file write offsets. In retrospect it might have been better to implement the reader and the writer in this program as read and write streams, but the queue logic is only a few lines of code.

  An odd thing cost me some time to chase down. The fswrite() routine would not accept writing to offsets and emitted a cryptic (and inaccurate) error message. It turns out that it wants to see the data as a Buffer. The request package returns data as a string by default, raw data is delivered as a Buffer only with encoding=null.