const expect = require("chai").expect;
const util = require('util');
const downloadFile = require('../dlf');

const goodURL = 'http://a6507e97.bwtest-aws.pravala.com/384MB.jar';
const badURL  = 'http://a6507e97.bwtest-aws.pravalaX.com/384MB.jar';

var args = {
    chunkSize: 1048576,
    maxChunks: 4,
    getAll: false,
    startIndex: 0,
    oFile: 'outputFile.dat',
    nParallel: 1,
    debug: false,
    url: goodURL
}

describe("mget tests", function() {
    this.timeout(1500000);
    it("default; 4 chunks, parallel = 1", async function() {
        let cargs = Object.assign({}, args);
        try {
            let dlfi = instantiate(cargs);
            let resultObj = await dlfi();
            //         console.log(`${res.writtenKBytes}kb written in ${res.seconds} seconds as ${res.nChunks} ${res.chunkSize} chunks`);
            expect(resultObj);
            expect(resultObj.nChunks).to.equal(4)
        }
        catch (err) {
            console.log(err);
        }
    });

    it("d4 chunks, parallel = 2", async function() {
        let cargs = Object.assign({}, args);
        cargs.nParallel = 2;
        try {
            let dlfi = instantiate(cargs);
            let resultObj = await dlfi();
            expect(resultObj);
            expect(resultObj.nChunks).to.equal(4)
        }
        catch (err) {
            console.log(err);
        }
    });

    it("bad URL", async function() {
        let cargs = Object.assign({}, args);
        cargs.url = badURL;
        try {
            let dlfi = instantiate(cargs);
            let resultObj = await dlfi();
        }
        catch (err) {
            // console.log(err);
            expect(err).to.equal('chunkResult: bad URL a6507e97.bwtest-aws.pravalax.com, exiting');
        }
    });
});

function instantiate(theArgs) {
    let dlf = downloadFile.setup(theArgs);
    let dlfi = util.promisify(dlf.initiate);
    return dlfi;
}